# My Wonderful Online Shop #

### Warning: ###
This app is **extremely vulnerable** and not even the slightest should be re-used anywhere for a working environment!

This app was created **for teaching and demonstrating** basic web attacks. And this is how it should be used.


### Install ###
It uses basic PHP and MySQL. Just clone the repo into your *localhost* and set the database up as follows:

* run the *.mysql* on the command line to create the database and fill it with same basic values
```
#!bash

mysql -u <username> -p < setup.mysql
mysql -u <username> -p < init_forum.mysql
```
* db_password.txt contains the password used by the vulnerable app
* to reset the database, simply run *reset_db.sh*


### Slides ###
The web app was created for a guest lecture as part of the
[Web Applications course](http://www.cse.chalmers.se/edu/course/DAT076/)
at
[Chalmers University of Technology](http://www.chalmers.se/).
The used slides can be found on
[slides.google.com](https://docs.google.com/presentation/d/124OM4JWdd6qYFoyb3UAxggGK2eRAs3JIPD27VNpbXeU/edit?usp=sharing)

### Problems? ###
Feel free to contact [me](https://danielhausknecht.eu).