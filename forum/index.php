<?php
require_once "../database/DBQuery.php";
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Web Shop</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="forum.css">
</head>
<body>

<?php
/* +--------------------------------+
 * | Users                          |
 * +-----+-------+----------+-------+
 * | _id | email | password | admin |
 * +--------------------------------+
 *
 * +--------------------------------+
 * | ForumEntries                   |
 * +-----+--------+-------+---------+
 * | _id | author | title | message |
 * +-----+--------+-------+---------+
//*/
?>

<div id="home_link"> <a href=".."><img src="../images/home.png" /></a> </div>
<div id="headline">My Wonderful Online Shop</div>

<?php
if (isset($_GET["msg"])) {
 $msg = $_GET["msg"];
 echo '<div id="notification" >'.$msg.'</div>';
}
?>

<form class="forum_entry" id="new_entry_form" action="add_entry.php" method="POST">
 <div class="entry_header">
 <table>
    <tr>
      <td><span>Email: </span></td>
      <td><input type="email" name="email" value="foo@bar.com" /></td>
    </tr>
    <tr>
      <td><span>Title: </span></td>
      <td><input type="text" name="title" value="Your Title" /></td>
    </tr>
  </table>
  </div>
  <div class="entry_msg">
    <textarea form="new_entry_form" name="message"></textarea>
  </div>
  <input id="submit_btn" type="submit" value="Post Entry"/>
</form>



<?php
$query_str = "SELECT * FROM ForumEntries";
$result = null;
try {
  $query = new DBQuery($query_str);
  $query->stmt->execute();
  $result = $query->stmt->get_result();
  $query->close();
} catch (Exception $e) {
  echo $e;
}
if ($result != null) {
  while ($row = $result->fetch_assoc()) {
    $output = "<div class=\"entry_header\">".PHP_EOL
             ."<span>Title: ".$row["title"]."</span>".PHP_EOL
             ."<span>Author: ".$row["author"]."</span>".PHP_EOL
             ."<span>Nr: ".$row["_id"]."</span>".PHP_EOL
             ."</div>".PHP_EOL
             ."<p class=\"entry_msg\">".$row["message"]."</p>";
    echo "<div class=\"forum_entry\">$output</div>".PHP_EOL;
  }
}

?>

<body>
</html>

