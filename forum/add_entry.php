<?php

require_once "../database/DBQuery.php";


if ((isset($_POST["email"]) === FALSE) OR empty($_POST["email"])) {
  header("Location: /vulnerable_app/forum/?msg='Please provide an email address.'");

} else if ((isset($_POST["title"]) === FALSE) OR empty($_POST["title"])) {
  header("Location: /vulnerable_app/forum/?msg='Please provide a title.'");
  
} else if ((isset($_POST["message"]) === FALSE) OR empty($_POST["message"])) {
  header("Location: /vulnerable_app/forum/?msg='Your message is empty!'");

} else {
  $email = $_POST["email"];
  $title = $_POST["title"];
  $message = $_POST["message"];

  $query = new DBQuery("INSERT INTO ForumEntries (author,title,message) VALUES (?,?,?)");
  $query->stmt->bind_param("sss",$email,$title,$message);
  $query->stmt->execute();
  $query->close();

  header("Location: /vulnerable_app/forum/");
}
?>
