#!/bin/bash

echo "DROP DATABASE IF EXISTS vulnerable_db" | mysql -u root
mysql -u root < setup.mysql
mysql -u root < init_forum.mysql
echo "USE vulnerable_db; SHOW TABLES;" | mysql -u root
