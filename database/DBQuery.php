<?php

class DBQuery {
  public $stmt;
  public $conn;

  public function __construct($query_str = "", $dbname = "vulnerable_db") {
    $servername = "localhost";
    $username = "vulnerable_app";
    $db_pwd = trim(file_get_contents(dirname(__FILE__)."/db_password.txt"));

    // Create connection
    $conn = new mysqli($servername, $username, $db_pwd, $dbname);
    if ($conn->connect_error) {
      throw new Exception('Failed to connect to DB: '+$conn->connect_error);
    }

    // init and prepare
    $this->stmt = $conn->stmt_init();
    $this->conn = $conn;
    if (strlen($query_str) > 0) {
      $this->stmt->prepare($query_str);
    }
  }

  public function prepare($stmt_str) {
    $this->stmt->prepare($stmt_str);
    return $this->stmt;
  }

  public function close() {
    $this->stmt->close();
    $this->conn->close();
  }
}

?>
