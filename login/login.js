function pwdVisibility() {
  var elem = document.getElementById("pwd_input");
  elem.type = (elem.type == "text") ? "password" : "text";
}

function initCheckbox() {
  var elem = document.getElementById("visible_input");
  elem.onchange = pwdVisibility;
}

if (document.readyState === "complete") {
    initCheckbox();
} else if (window.addEventListener) { // W3C standard
  window.addEventListener('load', initCheckbox, false); // NB **not** 'onload'
} else if (window.attachEvent) { // Microsoft
  window.attachEvent('onload', initCheckbox);
}
