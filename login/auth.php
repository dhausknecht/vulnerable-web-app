<?php
require_once "../database/DBQuery.php";



function login($email, $password) {
  $stmt_1 = "SELECT _id FROM Users WHERE email='$email' AND password='$password';";

  $conn = (new DBQuery())->conn;
  $conn->multi_query($stmt_1);
  $result = $conn->store_result();

  if ($result && $result->num_rows > 0) {
    // MySQL INT max, rand cryptographically not secure
    $authToken = rand(1, 2147483647);
    $row = $result->fetch_assoc();
    $id = $row["_id"];
    $stmt_2 = "INSERT INTO AuthTokens (authToken,user) VALUES($authToken,$id);";
    $insert_conn = (new DBQuery())->conn;
    $insert_conn->query($stmt_2);
    $insert_conn->close();

    setcookie("authToken",$authToken,time()+31536000,"/");
    header("Location: welcome.php");

  } else {
    header("Location: .");
  }
  $conn->close();

}


$isGET = isset($_GET["action"]);
if (! (isset($_GET["action"]) OR isset($_POST["action"]))) {
  header("Location: .");
  exit();
}

if (($_GET["action"] == "login") OR ($_POST["action"] == "login")) {
  $email = ($isGET) ? $_GET["email"] : $_POST["email"];
  $password = ($isGET) ? $_GET["password"] : $_POST["password"];

  login($email, $password);

} else if (($_GET["action"] == "logout") OR ($_POST["action"] == "logout")) {
  if (isset($_COOKIE["authToken"])) {
    $authToken = $_COOKIE["authToken"];

    $query = new DBQuery("DELETE FROM AuthTokens WHERE authToken=$authToken;");
    $query->stmt->execute();
    $query->close();
  }
  setcookie("authToken",0,0,"/");

  header("Location: .");
}
?>



















<?php
function prep_login($email, $password) {
  $stmt_1 = "SELECT _id FROM Users WHERE email=? AND password=?;";

  $query = new DBQuery();
  $stmt = $query->prepare($stmt_1);
  $stmt->bind_param("ss",$email,$password);
  $stmt->execute();
  $result = $stmt->get_result();

  if ($result && $result->num_rows > 0) {
    // MySQL INT max, rand cryptographically not secure
    $authToken = rand(1, 2147483647);
    $row = $result->fetch_assoc();
    $id = $row["_id"];
    $stmt_2 = "INSERT INTO AuthTokens (authToken,user) VALUES(?,?);";
    $query2 = new DBQuery();
    $stmt = $query2->prepare($stmt_2);
    $stmt->bind_param("ii",$authToken,$id);
    $stmt->execute();
    $query2->close();

    setcookie("authToken",$authToken, time()+31536000,'/');
    header("Location: welcome.php");

  } else {
    header("Location: .");
  }
  $query->close();

}
?>
