<?php

require_once "../database/DBQuery.php";

if (isset($_COOKIE["authToken"])) {
  $query = new DBQuery("SELECT email,password,admin FROM AuthTokens JOIN Users "
                      ."ON AuthTokens.authToken=? AND AuthTokens.user=Users._id;");
  $query->stmt->bind_param("i",$_COOKIE["authToken"]);
  $query->stmt->execute();
  $result = $query->stmt->get_result();
  $query->close();
  $row = $result->fetch_assoc();
  $name = $row["name"];
  $email = $row["email"];
  $isAdmin = ($row["admin"] == 1);

} else {
  header("Location: .");
  exit();
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Web Shop</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="login.css">
<script src="login.js"></script>
</head>
<body>

<div id="home_link"> <a href=".."><img src="../images/home.png" /></a> </div>
<div id="headline">My Wonderful Online Shop</div>

<div id="user_infos">
<div>
<h2>Welcome, <?php echo "$name" ?>!</h2>

<p>Your email address: <?php echo "$email" ?></p>
<p>You are <?php if (! $isAdmin) echo "<b>not</b> " ?>an admin.</p>
</div>

<form id="logout_form" action="auth.php" method="POST">
  <input type="hidden" name="action" value="logout" />
  <input type="submit" value="Logout" />
</form>
</div>

<body>
</html>

