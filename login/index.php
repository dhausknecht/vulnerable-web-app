<?php
if (isset($_COOKIE["authToken"])) {
  header("Location: welcome.php");
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Web Shop</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="login.css">
<script src="login.js"></script>
</head>
<body>

<div id="home_link"> <a href=".."><img src="../images/home.png" /></a> </div>
<div id="headline">My Wonderful Online Shop</div>


<form id="login_form" action="auth.php" method="POST">
 <input type="hidden" name="action" value="login" />
 <table>
    <tr>
      <td><span>Email: </span></td>
      <td><input id="email_input" type="email" name="email" value="foo@bar.com" /></td>
    </tr>
    <tr>
      <td><span>Password: </span></td>
      <td><input id="pwd_input" type="password" name="password" value="password" /></td>
    </tr>
    <tr>
      <td><input id="visible_input" type="checkbox"/><span> Make password visible</span></td>
    </tr>
  </table>

  <input id="submit_btn" type="submit" value="Login"/>
</form>

<body>
</html>

