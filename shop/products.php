<?php
class Product {
  public $name; public $price; public $ref_name;
  function __construct($name,$price,$ref_name) {
    $this->name = $name; $this->price = $price; $this->ref_name = $ref_name;
  }
}

$products = [
  new Product("sandcastle", 100, "sandcastle"),
  new Product("snowman", 200, "snowman"),
  new Product("Jar with Air", 50, "jar_air")
];

function findProduct($ref) {
  global $products;
  foreach ($products as $p) {
    if ($p->ref_name == $ref) { return $p; }
  }
  return null;
}
?>
