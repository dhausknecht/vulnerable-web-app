

function initSubmitClick() {
  var elem = document.getElementById("order_form");
  elem.onsubmit = function() {
    var inputs = document.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
      if ((inputs[i].type == "number") && (inputs[i].value < 0)) {
        inputs[i].value = 0;
      }
    }
  };
}


if (document.readyState === "complete") {
    initSubmitClick();
} else if (window.addEventListener) { // W3C standard
  window.addEventListener('load', initSubmitClick, false); // NB **not** 'onload'
} else if (window.attachEvent) { // Microsoft
  window.attachEvent('onload', initSubmitClick);
}
