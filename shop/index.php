<?php

require_once "products.php";


// process submitted data
$note = "<p>You bought: ";
$costs = 0;
$isFirst = true;
foreach ($_GET as $key => $amount) {
  if ($isFirst) { $isFirst = false; } else { $note .= ", "; }
  $p = findProduct($key);
  $costs += $p->price * $amount;
  $note .= $p->name.": $amount";
}
$note .= "</p><p>It costs: $costs SEK</p>";
?>



<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Web Shop</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="shop.css">
<!--script src="shop.js"></script-->
</head>
<body>

<div id="home_link"> <a href=".."><img src="../images/home.png" /></a> </div>
<div id="headline">My Wonderful Online Shop</div>

<div id="notification" ><?php echo $note ?></div>

<form id="order_form" action="" method="GET">
<div id="product_list">
<?php
for ($i = 0; $i < count($products); $i++) {
  $p = $products[$i];
  $result = 
   "  <div class='product'>".PHP_EOL
  ."    <div class='product_description'>".PHP_EOL
  ."      <p>".$p->name."</p>".PHP_EOL
  ."      <p>".$p->price." SEK</p>".PHP_EOL
  ."    </div>".PHP_EOL
  ."  <div class='product_count'>".PHP_EOL
  ."      <label for='p$i'>Buy: </label>".PHP_EOL

  ."      <input id='p$i' type='number' name='".$p->ref_name."' value='0' />".PHP_EOL /* min='0' */

  ."    </div>".PHP_EOL
  ."  </div>".PHP_EOL;
  echo $result;
}
?>
</div>

<div class="float_left">
  <input id="send_order" type="submit" value="Order"/>
</div>
</form>

<body>
</html>



























<?php
/*
if ($costs >= 0) {
  $note .= "</p><p>It costs: $costs SEK</p>";
} else {
  $note = "<p>Something went terribly wrong: $costs</p>";
}
//*/
?>

